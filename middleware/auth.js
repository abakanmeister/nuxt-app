export default function (context) {
    if (!context.store.getters.isAuthenticated) {
        console.log('just auth');
        context.redirect('/admin/auth')
    }
}